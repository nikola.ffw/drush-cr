# Auto Drush Cache

Node-based script that watches your project folder for any changes in .twig files and runs a cache-rebuild command in Drush.

## Install

```
1. clone the repository into Drupal's [theme] folder
2. run 'npm install' to install dependencies
3. in the 'tasks-node.js' file replace the 'folder: ghh' with the appropriate folder name for your project 
```

## Usage

```
run 'node tasks-node' in your terminal to start watching the changes in the .twig files
```

## Notes :warning:
```
Current version only supports 'Vagrant' setup.
```

## Contributing

PRs accepted.

## License

MIT © Nikola Jovanovic