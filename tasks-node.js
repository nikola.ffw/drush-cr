// NodeJS modules
const path = require('path');
const { exec } = require('child_process');

// External modules
const watch = require('node-watch');
const chalk = require('chalk');
const moment = require('moment');
const notifier = require('node-notifier');

// Script version
const version = {
  major: 0,
  minor: 0,
  patch: 1
};

// Script settings
const settings = {
  project: {
    // TODO: Make the script read the project dir name automatically
    folder: 'ghh'
  },
  messages: {
    init: 'Watching for changes...',
    type: {
      info: function (msg) {
        return chalk.yellow(msg)
      },
      error: function (msg) {
        return chalk.yellow(msg)
      },
      success: function (msg) {
        return chalk.yellow(msg)
      }
    }
  },
  notifier: {
    icon: path.join(process.cwd(), 'drush_logo.png')
  }
};

// Send a popup-styled message to the OS (notification)
const notify = msg => {
  notifier.notify({
    message: msg,
    icon: settings.notifier.icon
  });
};

// Get current time
const getTime = () => {
  return (
    moment().format('LTS')
  );
};

// Return a message with a time-stamp prepended [hh:mm:ss]
const setTimeStamp = msg => {
  return (`[${chalk.gray(getTime())}] ${msg}`);
};

// Send a terminal command to VM
const cmd = (command) => {
  // TODO: Script should detect project setup (vagrant/docksal) automatically
  console.log(
    setTimeStamp(`${settings.messages.type.info('Running')}: vagrant ssh -c "${command}"`)
  );
  exec(`vagrant ssh -c "${command}"`, (error, stdout, stderr) => {
    if (error) {
      notify(stderr);
      console.log(
        settings.messages.type.error(
          `error: ${error.message}`
        )
      );
      return;
    }
    if (stderr) {
      console.log(
        setTimeStamp(
          settings.messages.type.error(stderr)
        )
      );

      notify(stderr);
      return;
    }
    // Success
    notify(stderr);
    console.log(
      settings.messages.type.success(stdout)
    );
  });
};

// Task: watch
const watchTask = () => {
  console.log(
    settings.messages.type.info(
      settings.messages.init
    )
  );

  watch('./', {
      recursive: true,
      filter: /\.twig$/
    },
    function(evt, name) {
      console.log('%s changed.', name);
      // TODO: Auto-detect if there was a change on existing twig file, or was
      // a file added/removed (in the latter case "drush cr" is needed)
      cmd(`cd /var/www/${settings.project.folder}/ && drush cc render`)
    }
  );
}

// Functions to be invoked on initial run
const init = () => {
  // Set the local time settings to UK (avoids PM/AM)
  moment.locale('en-gb');
  // Watch for file changes
  watchTask();
};

// Initialize
init();
